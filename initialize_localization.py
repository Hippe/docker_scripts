import oerplib
import os
import sys

#Creates DB - uses arg 2 as mater password, assinges admin user password from arg 1
#Installs modules neede for Sprintit Promo Odoo Demo


ADDONS_TO_INSTALL=['account_accountant','mail']
SPRINTIT_ADDONS_TO_INSTALL=['l10n_fi','l10n_fi_reports','sprintit_company_information','sprintit_promo_data']
ADDON_PATH = "/mnt/extra-addons/"
DB_NAME = "odoo_promo"

ODOO_VERSION = os.environ["ODOO_VERSION"]
odoo_admin_password=sys.argv[1]
odoo_master_password=sys.argv[2]



#fetch sprintit modules from GIT (modules ned to be public)
for module in SPRINTIT_ADDONS_TO_INSTALL:
    os.system("mkdir  " + ADDON_PATH + module)
    os.system("git clone -b " + ODOO_VERSION + " https://bitbucket.org/sprintit/" + module + " " + ADDON_PATH + module)


oerp = oerplib.OERP(server='localhost',protocol='xmlrpc',port=8069)
oerp.db.create_database(odoo_master_password,DB_NAME,False,'en_US',odoo_admin_password)
user=oerp.login(user='admin',passwd=odoo_admin_password, database=DB_NAME)
module_obj = oerp.get('ir.module.module')
for module in SPRINTIT_ADDONS_TO_INSTALL + ADDONS_TO_INSTALL:
    module_id = module_obj.search([('name', '=', module)])
    module_obj.button_immediate_install(module_id)

